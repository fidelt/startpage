### Startpage
***
Startpage is simple homepage for your web browser that displays randomized rotary backgrounds.
By default, it uses DuckDuckGo as its search engine.

Check it out [here](https://bb.githack.com/fidelt/startpage/raw/ff1f431305bf00635aaea7ef89cdea0e71578520/index.html).

### Changing the Backgrounds
***
- Replace all the images found in the **backgrounds** directory with your images.
- Open the **index.html** file with a text editor.
- Now change the content of the **var backgrounds** to correspond to the names of your images.
```html
...
<script type="text/javascript">
    /* LIST YOUR IMAGES HERE */
    var backgrounds = [
        '1.jpg',
        '2.jpg',
        '3.jpg',
        ];
    
    function cycle_images() {
...
```
- Finally, save the changes that you have made.

### Changing the Search Engine
***
- Open the **index.html** file with a text editor.
- Navigate to this part of the file:
```html
...
<form action="https://duckduckgo.com/" method="get">
    <div id="search-group">
        <input type="text" id="search-input" name="q" autofocus>
        <button id="search-button">
            <img src="images/search.png">
        </button>
    </div>
</form>
...
```
- Change the 'action' attribute of the form tag to your preferred search engine.

| Search Engine | action                             |
| ------------- | ---------------------------------- |
| Bing          | `https://www.bing.com/search?`     |
| Google        | `https://www.google.com/search?`   |
| Yandex        | `https://www.yandex.com/?`         |

- Save the changes that you have made.